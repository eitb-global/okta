<?php

class Okta
{
    private $API_URL = "https://dev-57671111-admin.okta.com/";
    private $API_KEY = "00QPXLqf3E0P22Lis59J0poXrVNr63lNWYBI3xMi4o";

    public function register_service($name)
    {
        $jwk = $this->generateJWKS("./secret.pem");
        $data = array(
            "client_name" => $name,
            "response_types" => ["token"],
            "grant_types" => [
                "client_credentials",
            ],
            "token_endpoint_auth_method" => "private_key_jwt",
            "application_type" => "service",
            "jwks" => $jwk,
        );

        return $this->send_request("oauth2/v1/clients", "POST", $data);
    }

    /**
     * @param string $file File name with path
     * @return array
     */
    private function generateJWKS($file)
    {
        $keyInfo = openssl_pkey_get_details(openssl_pkey_get_public(file_get_contents($file)));
        $jsonData = [
            'keys' => [
                [
                    'kty' => 'RSA',
                    'n' => rtrim(str_replace(['+', '/'], ['-', '_'], base64_encode($keyInfo['rsa']['n'])), '='),
                    'e' => rtrim(str_replace(['+', '/'], ['-', '_'], base64_encode($keyInfo['rsa']['e'])), '='),
                ],
            ],
        ];

        return $jsonData;
    }

    /**
     * @param string $url
     * @param string $type
     * @param array $body optional param
     * @return mixed
     */
    private function send_request($url, $type = "GET", $body = "")
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->API_URL . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_HTTPHEADER => array(
                "Authorization: SSWS " . $this->API_KEY,
                'Content-Type: application/json',
                'Accept: application/json',
            ),
        ));

        if ($type == "POST") {
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($body));
        }

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;
    }
}

$okta = new Okta();

echo $okta->register_service("Fiverr");
